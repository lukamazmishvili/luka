package com.example.myapplication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.TextView
import kotlin.math.pow
import kotlin.math.sqrt

class MainActivity : AppCompatActivity() {

    private lateinit var resultTextView: TextView

    private var operand : Double = 0.0
    private var operand1 : Double = 0.0
    private var operation :  String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        resultTextView = findViewById(R.id.resultBox)

    }

    fun numberClick(clickedView: View){

        if (clickedView is TextView) {

            var result = resultTextView.text.toString()
            val number = clickedView.text.toString()

            if (result == "0"){

                result = ""
            }

            resultTextView.text = result + number

        }

    }

    fun operationClick(clickedView: View) {

        if (clickedView is TextView){

            val result = resultTextView.text.toString()

            if (result.isNotEmpty()){

                operand = result.toDouble()

            }

            operation = clickedView.text.toString()

            resultTextView.text = ""

            if (operation == "√"){

                resultTextView.text = (sqrt(operand)).toString()

                if (resultTextView.text.endsWith(".0")){

                    resultTextView.text = resultTextView.text.toString().dropLast(2)
                }

            }

        }

    }

    fun equalsClick(clickedView: View) {

        var secondOperandText = resultTextView.text.toString()
        var secondOperand : Double = 0.0

        if (secondOperandText.isNotEmpty()){

            secondOperand = secondOperandText.toDouble()
        }

        if(operation == "+"){

                resultTextView.text = (operand + secondOperand).toString()

            if (resultTextView.text.endsWith(".0")){

                resultTextView.text = resultTextView.text.toString().dropLast(2)
            }

        }

        if(operation == "-"){

            resultTextView.text = (operand - secondOperand).toString()

            if (resultTextView.text.endsWith(".0")){

                resultTextView.text = resultTextView.text.toString().dropLast(2)
            }

        }

        if(operation == "×"){

            resultTextView.text = (operand * secondOperand).toString()

            if (resultTextView.text.endsWith(".0")){

                resultTextView.text = resultTextView.text.toString().dropLast(2)
            }

        }

        if(operation == "÷"){

            resultTextView.text = (operand / secondOperand).toString()

            if (secondOperandText == "0"){

                resultTextView.text = ("Cannot divide by Zero")

            }


            if (resultTextView.text.endsWith(".0")){

                resultTextView.text = resultTextView.text.toString().dropLast(2)
            }

        }


        if (operation == "%"){

            resultTextView.text = (operand * (secondOperand / 100)).toString()

            if (resultTextView.text.endsWith(".0")){

                resultTextView.text =resultTextView.text.toString().dropLast(2)

            }
        }

        if (operation == "xⁿ"){

            resultTextView.text = (operand.pow(secondOperand)).toString()


            if (resultTextView.text.endsWith(".0")){

                resultTextView.text =resultTextView.text.toString().dropLast(2)

            }
        }

    }


    fun clearClick(clickedView: View){

        val value = 0
        operand = 0.0
        operand1 = 0.0

        if (value == 0){

            resultTextView.setText("")
        }

    }

    fun delClick(clickedView: View){

        val text = resultTextView.text.toString()

        if (text.isNotEmpty()){
            resultTextView.text = text.dropLast(1)
        }
    }

    fun dotClick(clickedView: View){

        val result = resultTextView.text.toString()

        if ("." !in result && result.isNotEmpty()){

            resultTextView.text = "$result."

        }
    }


}

